package ru.itis.formulas.function

import kotlin.math.*
import ru.itis.formulas.model.Planet
import ru.itis.formulas.utils.Constant.Companion.GRAVITATIONAL_CONSTANT
import ru.itis.formulas.utils.Constant.Companion.MAGNETIC_CONSTANT
import ru.itis.formulas.utils.SolarSystemPlanets

class WeightCalculation {

    /**
     * Формула веса на планете X
     * @param massObject масса тела на Земле (в кг)
     * @param planet планета X
     * @return вес на планете Х
     */
    fun calculateWeight(
        planet: Planet,
        massObject: Double,
    ): Double {
        return calculateWeightOnPlanetByGravitationalAcceleration(massObject, planet)
    }

    /**
     * Формула веса через усорение свободного падения
     * @param massObject масса тела
     * @param planet объект планеты X
     * @return вес на планете Х
     */
    fun calculateWeightOnPlanetByGravitationalAcceleration(
        massObject: Double,
        planet: Planet,
    ): Double {
        return (massObject * planet.gravitationalAcceleration) / SolarSystemPlanets.EARTH.planet.gravitationalAcceleration
    }

    /**
     * Поиск веса на объектах Солнечной системы
     * @param massObject масса тела
     * @param namePlanet название объекта Солнечной системы
     * @return вес на планете
     */
    fun calculateWeight(
        namePlanet: String,
        massObject: Double,
    ): Double {
        return when (namePlanet) {
            SolarSystemPlanets.EARTH.name -> calculateWeight(SolarSystemPlanets.EARTH.planet, massObject)
            SolarSystemPlanets.MARS.name -> calculateWeight(SolarSystemPlanets.MARS.planet, massObject)
            SolarSystemPlanets.MERCURY.name -> calculateWeight(SolarSystemPlanets.MERCURY.planet, massObject)
            SolarSystemPlanets.MOON.name -> calculateWeight(SolarSystemPlanets.MOON.planet, massObject)
            SolarSystemPlanets.VENUS.name -> calculateWeight(SolarSystemPlanets.VENUS.planet, massObject)
            SolarSystemPlanets.JUPITER.name -> calculateWeight(SolarSystemPlanets.JUPITER.planet, massObject)
            SolarSystemPlanets.SATURN.name -> calculateWeight(SolarSystemPlanets.SATURN.planet, massObject)
            SolarSystemPlanets.URANUS.name -> calculateWeight(SolarSystemPlanets.URANUS.planet, massObject)
            SolarSystemPlanets.NEPTUNE.name -> calculateWeight(SolarSystemPlanets.NEPTUNE.planet, massObject)
            SolarSystemPlanets.PLUTO.name -> calculateWeight(SolarSystemPlanets.PLUTO.planet, massObject)
            SolarSystemPlanets.SUN.name -> calculateWeight(SolarSystemPlanets.SUN.planet, massObject)
            else -> throw IllegalArgumentException("The planet is not found")
        }
    }

    /**
     * @param massObject масса тела
     * @param planet объект планеты X
     * @return вес на планете Х
     */
    fun calculateWeightOnPlanetByFirstSpaceVelocity(
        massObject: Double,
        planet: Planet
    ): Double {
        return (massObject * planet.firstSpaceVelocity.pow(2)) / (GRAVITATIONAL_CONSTANT * planet.radius.pow(2))
    }

    /**
     * @param massObject масса тела
     * @param planet объект планеты X
     * @return вес на планете Х
     */
    fun calculateWeightOnPlanetBySecondSpaceVelocity(
        massObject: Double,
        planet: Planet
    ): Double {
        return (massObject * planet.secondSpaceVelocity.pow(2)) / (2 * GRAVITATIONAL_CONSTANT * planet.radius)
    }

    /**
     * Формула веса через гравитационный потенциал
     * @param massObject масса тела
     * @param planet объект планеты X
     * @param f гравитационный потенциал планеты
     * @return вес на планете Х
     */
    fun calculateWeightOnPlanetByGravitationalPotential(
        massObject: Double,
        planet: Planet,
        f: Double,
    ): Double {
        return (massObject * f) / planet.gravitationalAcceleration
    }

    /**
     * Формула веса через плотность газа
     * @param massObject масса тела
     * @param planet объект планеты X
     * @param gravitationalAccelerationEarth усорение свободного падения на Земле
     * @param density плотность газа в атмосфере
     * @return вес на планете Х
     */
    fun calculateWeightOnPlanetByGasDensity(
        massObject: Double,
        planet: Planet,
        gravitationalAccelerationEarth: Double,
        density: Double
    ): Double {
        return (massObject * planet.gravitationalAcceleration) / (gravitationalAccelerationEarth * density)
    }

    /**
     * Формула веса через гравитационное эксцентриситет, момент инерции, широту
     * @param massObject масса тела
     * @param planet объект планеты X
     * @param w угловая скорость вращения планеты
     * @param eccentricity эксцентриситет
     * @param momentOfInertia момент инерции
     * @param latitude широта
     * @return вес на планете Х
     */
    fun calculateWeightOnPlanetByEccentricityAndMomentOfInertia(
        massObject: Double,
        planet: Planet,
        w: Double,
        eccentricity: Double,
        momentOfInertia: Double,
        latitude: Double
    ): Double {
        return massObject *
                (w.pow(2) * planet.radius.pow(2) - 2 * GRAVITATIONAL_CONSTANT * planet.mass * planet.radius - eccentricity * momentOfInertia.pow(2) * cos(latitude) * cos(latitude) / planet.radius.pow(2)) /
                (w.pow(2) * planet.radius.pow(2) - 2 * GRAVITATIONAL_CONSTANT * planet.mass * planet.radius - eccentricity * momentOfInertia.pow(2) * sin(latitude) * sin(latitude) / planet.radius.pow(3))
    }

    /**
     * Формула веса через гравитационное воздействие планеты, атмосферу и магнитное поле
     * @param massObject масса тела на Земле (в кг)
     * @param planet объект планеты X
     * @param height высота
     * @param radiusEarth радиус Земли
     * @param bo значение магнитного поля планеты
     * @param latitude широта
     * @return вес на планете Х
     */
    fun calculateWeightOnPlanetByMagnetic(
        massObject: Double,
        planet: Planet,
        height: Double,
        radiusEarth: Double,
        bo: Double,
        latitude: Double
    ): Double {
        return massObject * (GRAVITATIONAL_CONSTANT * planet.mass / ((planet.radius + height).pow(2))) * (1 - (2 * radiusEarth * bo.pow(2) * planet.radius.pow(6) * sin(latitude).pow(2)) / (3 * MAGNETIC_CONSTANT * (planet.radius + height).pow(4)))
    }
}

package ru.itis.formulas.utils

import ru.itis.formulas.model.Planet

enum class SolarSystemPlanets(val planet: Planet) {
    EARTH(Planet(6378.14, 5.972e24, 9.8)),
    MERCURY(Planet(2439.0, 3.285e23, 3.7)),
    VENUS(Planet(6051.5, 4.867e24, 8.87)),
    MARS(Planet(3397.4, 6.39e23, 3.721)),
    JUPITER(Planet(69911.0, 1.898e27, 24.8)),
    SATURN(Planet(58232.0, 5.683e26, 10.4)),
    URANUS(Planet(25362.0, 8.681e25, 8.87)),
    NEPTUNE(Planet(24622.0, 1.024e26, 10.15)),
    PLUTO(Planet(1195.0, 1.303e22, 0.66)),
    MOON(Planet(2736.9, 7.342e22, 1.62)),
    SUN(Planet(695500.0, 2e30, 274.0))
}

package ru.itis.formulas.utils
import kotlin.math.*

class Constant {

    companion object {
        const val GRAVITATIONAL_CONSTANT = 6.673e-11
        const val MAGNETIC_CONSTANT = 4e-7 * PI
    }

}
